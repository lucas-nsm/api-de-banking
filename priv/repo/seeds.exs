# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Bank.Repo.insert!(%Bank.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

#{:ok, a_user}        = Bank.Accounts.create_user(%{email: "auser@email.com", password: "pass_a"})
#{:ok, a_account}     = Bank.Accounts.create_account(a_user, %{identity: "1234", balance: 1000})
#{:ok, b_account}     = Bank.Accounts.create_account(a_user, %{identity: "1235", balance: 0})
#{:ok, _} = Bank.Accounts.withdraw(a_account, %{amount: "300.21"})
#{:ok, _} = Bank.Accounts.withdraw(a_account, %{amount: "100,66323"})
#{:ok, _} = Bank.Accounts.withdraw(a_account, %{amount: "100,2312341"})
#{:ok, _} = Bank.Accounts.deposit(a_account, %{amount: "123,123123"})
#{:ok, _} = Bank.Accounts.transfer(a_account, %{amount: "100", dst_account: 1235})
#{:ok, _} = Bank.Accounts.transfer(a_account, %{amount: "100,1", dst_account: 1235})
#{:ok, _} = Bank.Accounts.transfer(a_account, %{amount: "100,1231234", dst_account: 1235})
#
#{:ok, b_user}        = Bank.Accounts.create_user(%{email: "buser@email.com", password: "pass_b"})
#{:ok, b_account}     = Bank.Accounts.create_account(b_user)
##{:ok, b_transaction} = Bank.Accounts.create_transaction(b_account, %{amount: "800"})

