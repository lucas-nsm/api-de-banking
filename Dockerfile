FROM elixir:latest

RUN apt-get update && \
  apt-get install -y postgresql-client

RUN mkdir /app
COPY . /app
WORKDIR /app

RUN rm -rf /app/_build/*

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix do compile

CMD mix ecto.create && mix ecto.migrate && mix phx.server
