# Desafio Técnico - API de Banking

[[_TOC_]]


## Introdução

O sistema deve oferecer a possibilidade de usuários realizarem transações financeiras como saque e transferencia entre contas.

Um usuário pode se cadastrar e ao completar o cadastro ele recebe R$ 1000,00.

Com isso ele pode transferir dinheiro para outras contas e pode sacar dinheiro. O saque do dinheiro simplesmente manda um email para o usuário informando sobre o saque e reduz o seu saldo (o envio de email não precisa acontecer de fato, pode ser apenas logado e colocado como "placeholder" para envio de email de fato).

Nenhuma conta pode ficar com saldo negativo.

É necessário autenticação para realizar qualquer operação.

É preciso gerar um relatório no backoffice que dê o total transacionado (R$) por dia, mês, ano e total.

## Requisitos Técnicos

* O desafio deve ser feito na linguagem Elixir.
* A API deve utilizar JSON (i.e.: Accept e Content-type)
* O uso de Docker é obrigatório.

## 1. Desenvolvimento

Para atender aos requisitos, a API foi desenvolvida com as seguintes recursos:

- Framework [Phoenix](https://www.phoenixframework.org/) do Elixir. 
- Biblioteca de autenticação [Guardian](https://github.com/ueberauth/guardian). 
- Plataforma para deploy [Gigalixir](https://www.gigalixir.com/).


## 2. Funcionamento

O sistema foi desenvolvido para realizar simples operações bancárias por meio da 
API REST desenvolvida em Elixir.

Todas as operações precisam ser executadas com autenticação de usuário previamente estabelecida.

### 2.1. API Endpoints

#### 2.1.1. POST /api/users/signup

Cria um  usuário usando credenciais de `email` e `password` e gera um token de autenticação.

 - **Parameters**

Nome       |  Tipo  | Descrição               | Exemplo         |
-----------|--------|-------------------------|-----------------|
`email`    | string | email                   | auser@email.com |
`password` | string | senha(mínimo 6 digitos) | pass_a          |

 - **Resposta**

```json
{
  "email": "auser@email.com",
  "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9..."
}
```

#### 2.1.2. POST /api/users/signin

Autentica o usuário usando credenciais de `email` e `password` e gera um token de autenticação.

 - **Parameters**

Nome       |  Tipo  | Descrição               | Exemplo         |
-----------|--------|-------------------------|-----------------|
`email`    | string | email                   | auser@email.com |
`password` | string | senha(mínimo 6 digitos) | pass_a          |

 - **Resposta**

```json
{
  "email": "auser@email.com",
  "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9..."
}
```

#### 2.1.3. POST /api/accounts/create

Cria uma conta para o usuário.

 - **Resposta**

```json
{
   "data" : {
      "identity" : "RXNIUP",
      "balance" : "1000.00"
   }
}
```
#### 2.1.4. GET /api/accounts

Mostra as informações das contas do usuário.

 - **Resposta**

```json
{
   "data" : [
      {
         "balance" : "0.00",
         "identity" : "MFNPCL"
      },
      {
         "balance" : "1000.00",
         "identity" : "RXNIUP"
      }
   ]
}
```
#### 2.1.5. GET /api/accounts/:identity

Mostra as informações da conta do usuário.

 - **Parameters**

Nome       |  Tipo  | Descrição               | Exemplo         |
-----------|--------|-------------------------|-----------------|
`identity` | string | conta                   | MFNPCL          |

 - **Resposta**

```json
{
   "data" : {
      "identity" : "MFNPCL",
      "balance" : "0.00"
   }
}
```
#### 2.1.6. POST /api/transactions/withdraw/:identity

Retira dinheiro da conta.

 - **Parameters**

Nome       |  Tipo  | Descrição               | Exemplo         |
-----------|--------|-------------------------|-----------------|
`identity` | string | conta                   | MFNPCL          |
`amount`   | string | dinheiro                | 800.29          |

 - **Resposta**

```json
{
   "data" : {
      "id" : "253ca311-53b6-4dcc-ab41-8fab122b6a75",
      "date" : "2020-10-05 22:35:19",
      "amount" : "-800.29"
   }
}
```
#### 2.1.7. POST /api/transactions/deposit/:identity

Deposita dinheiro na conta.

 - **Parameters**

Nome       |  Tipo  | Descrição               | Exemplo         |
-----------|--------|-------------------------|-----------------|
`identity` | string | conta                   | 6RTM7Q          |
`amount`   | string | dinheiro                | 100.56          |

 - **Resposta**

```json
{
   "data" : {
      "id" : "162d9f34-d162-4114-9322-0f04ab5c81e9",
      "date" : "2020-10-05 22:39:13",
      "amount" : "100.56"
   }
}
```

#### 2.1.8. POST /api/transactions/transfer/:identity

Transfere dinheiro para outra conta.

 - **Parameters**

Nome           |  Tipo  | Descrição               | Exemplo         |
---------------|--------|-------------------------|-----------------|
`identity`     | string | conta                   | MFNPCL          |
`amount`       | string | dinheiro                | 183.99          |
`dst_identity` | string | conta de destino        | GHYPCL          |

 - **Resposta**

```json
{
   "data" : {
      "id" : "a20bdd4b-f557-42e5-8a88-e361abcd58d8",
      "date" : "2020-10-05 22:40:56",
      "amount" : "-183.99"
   }
}
```
#### 2.1.9. GET /api/transactions/:identity/:period

Mostra transações realizadas na conta em um período informado.

 - **Parameters**

Nome           |  Tipo  | Descrição      | Exemplo              |
---------------|--------|----------------|----------------------|
`identity`     | string | conta          | 6RTM7Q               |
`period`       | string | período (se não informado, mostrará todos),<br>formato="?date=**ANO**%2D**MÊS**%2D**DIA**"     | ?date=2020%2D10%2D05<br>?date=2020%2D10<br>?date=2020<br> |



 - **Resposta**

```json
{
   "total" : "1284.55",
   "period" : "total",
   "identity" : "6RTM7Q"
}
```
### 2.2. Retornos de erro

 - **Resposta**

```json
{
   "errors" : {
     "detail" : erro
   }
}
```

- Lista de erros:

Erro                 |  Descrição                          |
---------------------|-------------------------------------| 
`Insufficient funds` | sem saldo para realizar a operação  |
`Not Found`          | conta ou usuário não foi encontrado |
`Unauthorized`       | sem autoriação para a requisição    |
`Invalid format`     | formato inválido                    |

## 3. Configuração

A API pode ser executada pelo **servidor local** ou por meio do `Docker`

 - `Docker`:

   ```shell
   docker-compose up
   ```

 - **Servidor local**:
   ```shell
   mix deps.get
   mix phx.server
   ```

## 4. Testes

Os testes são executados automaticamente sempre que um novo commit entra no ***master***. Mas também podem ser executados localmente por meio do comando:

```shell
mix test
```

Também é possível testar a API por meio do comando `curl`, para tanto há a script de teste [run_test.sh](https://gitlab.com/lucas-nsm/api-de-banking/-/blob/master/run_test.sh) que possui alguns comandos já prontos para executar.

## 5. Deploy

O deploy é feito automaticamente para o ***Staging environment*** sempre que um novo commit entra no ***master***. 
> Consulte o arquivo [.gitlab-ci.yml](https://gitlab.com/lucas-nsm/api-de-banking/-/blob/master/.gitlab-ci.yml).<br>
> Consulte os pipelines em [Pipelines](https://gitlab.com/lucas-nsm/api-de-banking/-/pipelines). :rocket: 

### 5.1. Staging environment

A réplica do ambiente de produção está disponível em:

 - https://plush-normal-sphinx.gigalixirapp.com

## 6. Futuras Melhorias

 - Fazer melhor uso do modulo **Logger**, juntamente com o recurso **Metadata**, para prover logs mais adequados para os ambientes de **dev** e **prod**.
 - Implementar o metodo **REST API DELETE** para remover registros, levando em consideração a dependência de cada tabela.
 - Fazer melhor uso dos **Typespecs**.
 - Fazer melhor uso dos tratamentos de exceções.

## Referências:
 - https://elixirschool.com/pt/
 - https://medium.com/@pamit/building-a-restful-backend-with-elixir-phoenix-84fe390975c
 - https://medium.com/@njwest/jwt-auth-with-an-elixir-on-phoenix-1-3-guardian-api-and-react-native-mobile-app-1bd00559ea51
 - https://lobotuerto.com/blog/deploying-an-elixir-phoenix-application-to-gigalixir/
 - https://elixircasts.io/user-authentication-with-phoenix
 - https://hexdocs.pm/phoenix/gigalixir.html
 - https://pspdfkit.com/blog/2018/how-to-run-your-phoenix-application-with-docker/


