
withdraw() {
    token=${1}
    identity=${2}
    amount=${3}
    echo "[withdraw] account=${identity}, amount=${amount}"
    curl -X POST \
    ${host}/api/transactions/withdraw/${identity} \
    -H "Content-Type: application/json" -d '{"amount": "'"${amount}"'"}' \
    -H "Authorization: Bearer ${token}" \
    | json_pp
}

deposit() {
    token=${1}
    identity=${2}
    amount=${3}
    echo "[deposit] account=${identity}, amount=${amount}"
    curl -X POST \
    ${host}/api/transactions/deposit/${identity} \
    -H "Content-Type: application/json" -d '{"amount": "'"${amount}"'"}' \
    -H "Authorization: Bearer ${token}" \
    | json_pp
}

transfer() {
    token=${1}
    identity=${2}
    dst_identity=${3}
    amount=${4}
    echo "[transfer] account=${identity}, amount=${amount}, dst_account=${dst_identity}"
    curl -X POST \
    ${host}/api/transactions/transfer/${identity} \
    -H "Content-Type: application/json" -d '{"amount": "'"${amount}"'", "dst_account": "'"${dst_identity}"'"}' \
    -H "Authorization: Bearer ${token}" \
    | json_pp
}

create_user() {
    email=${1}
    pass=${2}
    echo "[create_user] email=${email}"
    curl -X POST \
    ${host}/api/users/signup \
    -H "Content-Type: application/json" \
    -d '{"email": "'"${email}"'", "password": "'"${pass}"'"}'
}

login() {
    email=${1}
    pass=${2}
    token=`curl -X POST \
    ${host}/api/users/signin \
    -H "Content-Type: application/json" \
    -d '{"email": "'"${email}"'", "password": "'"${pass}"'"}'\
    | jshon -e token -u`
    echo ${token}
}

create_account() {
    token=${1}
    identity=`curl -X POST \
    ${host}/api/accounts/create \
    -H "Authorization: Bearer ${token}" \
    | jshon -e data -e identity -u`
    echo ${identity}
}

show_account() {
    token=${1}
    identity=${2}
    curl -X GET \
    ${host}/api/accounts/${identity} \
    -H "Authorization: Bearer ${token}" \
    | json_pp
}

get_one_account() {
    token=${1}
    account=`curl -X GET \
    ${host}/api/accounts \
    -H "Authorization: Bearer ${token}" \
    | jshon -e data -e 0 -e identity -u`
    echo ${account}
}

show_transactions() {
    token=${1}
    identity=${2}
    period=${3}
    curl -X GET \
    ${host}/api/transactions/${identity}/${period} \
    -H "Authorization: Bearer ${token}" \
    | json_pp
}


#########################################
# main
#########################################

a_email="auser@email.com"
a_pass="pass_a"

b_email="buser@email.com"
b_pass="pass_b"

host="http://localhost:4000"
#host="https://plush-normal-sphinx.gigalixirapp.com"

token_a=`login "${a_email}" "${a_pass}"`
if [ -z "${token_a}" ]; then
    create_user "${a_email}" "${a_pass}"
    token_a=`login "${a_email}" "${a_pass}"`
    account_a=`create_account "${token_a}"`
    show_account "${token_a}" "${account_a}"
fi

token_b=`login "${b_email}" "${b_pass}"`
if [ -z "${token_b}" ]; then
    create_user "${b_email}" "${b_pass}"
    token_b=`login "${b_email}" "${b_pass}"`
    account_b=`create_account "${token_b}"`
    show_account "${token_b}" "${account_b}"
fi
account_a=`get_one_account "${token_a}"`
account_b=`get_one_account "${token_b}"`
withdraw "${token_a}" "${account_a}" "800.29"
deposit "${token_b}" "${account_b}" "100.56"
transfer "${token_a}" "${account_a}" "${account_b}" "183.99"
show_transactions "${token_a}" "${account_a}" ""
show_transactions "${token_b}" "${account_b}" "?date=2020%2D09"
show_transactions "${token_b}" "${account_b}" ""
transfer "${token_a}" "${account_a}" "${account_b}" "200" 
