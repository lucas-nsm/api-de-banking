# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :bank,
  ecto_repos: [Bank.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :bank, BankWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "t7Zmr7Cfbf9C2WLwWvfGFgVsCLQU6LNR+9iGnj7qjemVGSKR5yCxa9z6z9ayfVko",
  render_errors: [view: BankWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Bank.PubSub,
  live_view: [signing_salt: "GPfJ3CgP"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

config :bank, BankWeb.Auth.Guardian,
  issuer: "bank",
  secret_key: "a4jXyJO/3fxlxJIG8ZbareDdjp0ftbluCyAVlnTzvnCzrVDG8bE0AKDsgagfy5Jc"
