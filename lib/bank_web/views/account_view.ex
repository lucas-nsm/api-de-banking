defmodule BankWeb.AccountView do
  use BankWeb, :view
  alias BankWeb.AccountView

  def render("index.json", %{accounts: accounts}) do
    %{data: render_many(accounts, AccountView, "account.json")}
  end

  def render("show.json", %{account: account}) do
    %{data: render_one(account, AccountView, "account.json")}
  end

  def render("account.json", %{account: account}) do
    %{balance: account.balance |> Decimal.round(2),
      identity: account.identity}
  end
end
