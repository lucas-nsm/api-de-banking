defmodule BankWeb.TransactionView do
  use BankWeb, :view
  alias BankWeb.TransactionView

  def render("index.json", %{transactions: transactions}) do
    %{data: render_many(transactions, TransactionView, "transaction.json")}
  end

  def render("show.json", %{transaction: transaction}) do
    %{data: render_one(transaction, TransactionView, "transaction.json")}
  end

  def render("transaction.json", %{transaction: transaction}) do
    %{id: transaction.id,
      amount: transaction.amount |> Decimal.round(2),
      date: NaiveDateTime.to_string(transaction.inserted_at)
    }
  end
  def render("total.json", %{identity: identity, period: period, total: total}) do
    %{
      identity: identity,
      period: period,
      total: total |> Decimal.round(2)
    }
  end
end
