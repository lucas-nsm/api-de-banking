defmodule BankWeb.TransactionController do
  use BankWeb, :controller

  alias Bank.Mailer
  alias Bank.Accounts
  alias Bank.Transactions
  alias Bank.Accounts.Transaction

  action_fallback BankWeb.FallbackController
  require Logger

  @bank_email "api_banking@email.com"

  def create(conn, f, params, email \\ %{}) do
    try do
      account =
        Guardian.Plug.current_resource(conn)
        |> Accounts.get_by!(:identity, params["identity"])

      attrs = for {key, val} <- params, into: %{}, do: {String.to_atom(key), val}

      with {:ok, %Transaction{} = transaction} <- f.(account, attrs) do

        if email != %{} do
          text =
            email.text
            |> String.replace("@AMOUNT@", "#{Decimal.mult(transaction.amount, -1)}")
            |> String.replace("@IDENTITY@", account.identity)
            |> String.replace("@DATE@", "#{account.inserted_at}")

          Map.merge(email, %{text: text, to: account.user.email}) |> Mailer.send_email()
        end

        conn
        |> put_status(:created)
        |> render("show.json", transaction: transaction)

      end
    rescue
      Ecto.NoResultsError ->
        {:error, :not_found}
    end
  end

  def deposit(conn, params) do
    Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: params='#{inspect(params)}'")
    create(conn, &Transactions.deposit/2, params)
  end

  def withdraw(conn, params) do
    Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: params='#{inspect(params)}'")
    email = %{
      from: @bank_email,
      subject: "Saque realizado com sucesso",
      text: "Saque de '@AMOUNT@' realizado na conta '@IDENTITY@' em '@DATE@'"
    }
    create(conn, &Transactions.withdraw/2, params, email)
  end

  def transfer(conn, params) do
    Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: params='#{inspect(params)}'")
    create(conn, &Transactions.transfer/2, params)
  end

  def show(conn, %{"identity" => identity} = params) do
    Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: params='#{inspect(params)}'")
    date = Map.get(params, "date", "")

    try do
      with {:ok, total} <- Guardian.Plug.current_resource(conn)
                                  |> Accounts.get_by!(:identity, identity)
                                  |> Transactions.get_total(date) do

        period = if byte_size(date) == 0, do: "total", else: date
        total = if total == nil, do: "0", else: total
        conn
        |> render("total.json", identity: identity, period: period, total: total)
      end
    rescue
      _error ->
        {:error, :invalid_format}

    catch kind, error ->
      Logger.error("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: #{kind}, #{error}")
      {:error, :invalid_format}
    end
  end
end
