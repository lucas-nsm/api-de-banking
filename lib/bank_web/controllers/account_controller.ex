defmodule BankWeb.AccountController do
  use BankWeb, :controller

  alias Bank.Accounts
  alias Bank.Transactions
  alias Bank.Accounts.Account

  action_fallback BankWeb.FallbackController
  require Logger

  def index(conn, _params) do
    Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))}")

    accounts = 
      Guardian.Plug.current_resource(conn)
      |> Accounts.list()

    render(conn, "index.json", accounts: accounts)
  end

  def create(conn, _params) do
    user = Guardian.Plug.current_resource(conn)
    account_params =  %{identity: Accounts.gen_identity(), balance: Decimal.new("0")}
    Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))}")

    with {:ok, %Account{} = account} <- Accounts.create(user, account_params) do

      if length(Accounts.list(user)) == 1 do
        Transactions.deposit(account, %{amount: "1000"})
      end

      conn
      |> put_status(:created)
      |> render("show.json", account: account)

    end
  end

  def show(conn, %{"identity" => identity}) do
    Logger.debug("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: identity='#{inspect(identity)}'")

    try do
      account = 
        Guardian.Plug.current_resource(conn)
        |> Accounts.get_by!(:identity, identity)
  
      render(conn, "show.json", account: account)
    rescue
      Ecto.NoResultsError ->
        {:error, :not_found}
    end
  end
end
