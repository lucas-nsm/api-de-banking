defmodule BankWeb.UserController do
  use BankWeb, :controller

  alias Bank.Users
  alias Bank.Accounts.User
  alias BankWeb.Auth.Guardian

  action_fallback BankWeb.FallbackController
  require Logger

  def signin(conn, user_params) do
    Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: params='#{inspect(user_params)}'")

    with {:ok, user, token} <- Guardian.authenticate(user_params["email"], user_params["password"]) do
      conn
      |> put_status(:created)
      |> render("user.json", %{user: user, token: token})
    end
  end

  def create(conn, user_params) do
    Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: params='#{inspect(user_params)}'")

    with {:ok, %User{} = user} <- Users.create(user_params),
    {:ok, token, _claims} <- Guardian.encode_and_sign(user) do
      conn
      |> put_status(:created)
      |> render("user.json", %{user: user, token: token})
    end
  end
end
