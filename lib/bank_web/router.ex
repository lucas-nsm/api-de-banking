defmodule BankWeb.Router do
  use BankWeb, :router

  pipeline :auth do
    plug BankWeb.Auth.Pipeline
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", BankWeb do
    pipe_through :api
    post "/users/signup", UserController, :create
    post "/users/signin", UserController, :signin
  end

  scope "/api", BankWeb do
    pipe_through [:api, :auth]

    post "/accounts/create", AccountController, :create
    get "/accounts", AccountController, :index
    resources "/accounts", AccountController, only: [:show], param: "identity"

    post "/transactions/withdraw/:identity", TransactionController, :withdraw
    post "/transactions/deposit/:identity", TransactionController, :deposit
    post "/transactions/transfer/:identity", TransactionController, :transfer
    resources "/transactions", TransactionController, only: [:show], param: "identity"
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: BankWeb.Telemetry
    end
  end
end
