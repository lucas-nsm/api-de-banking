defmodule Bank.Accounts.Transaction do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "transactions" do
    field :amount, :decimal
    belongs_to :account, Bank.Accounts.Account

    timestamps()
  end

  @doc false
  def changeset(transaction, account, attrs) do
    transaction
    |> cast(attrs, [:amount])
    |> validate_required([:amount])
    |> put_assoc(:account, account)
    |> validate_required([:account])
  end
end
