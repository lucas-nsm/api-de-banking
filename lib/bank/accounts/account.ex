defmodule Bank.Accounts.Account do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "accounts" do
    field :balance, :decimal
    field :identity, :string
    belongs_to :user, Bank.Accounts.User
    has_many :transactions, Bank.Accounts.Transaction

    timestamps()
  end

  @doc false
  def changeset(account, user, attrs) do
    account
    |> cast(attrs, [:balance, :identity])
    |> validate_required([:balance, :identity])
    |> put_assoc(:user, user)
    |> validate_required([:user])
    |> unique_constraint(:identity)
  end
end
