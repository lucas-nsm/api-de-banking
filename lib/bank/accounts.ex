defmodule Bank.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Bank.Repo
  alias Bank.Accounts.Account
  alias Bank.Accounts.User
  require Logger

  @doc """
  Returns the list of accounts.

  ## Examples

      iex> list()
      [%Account{}, ...]

  """
  def list do
    Repo.all(Account)
    |> Repo.preload(:user)
  end

  @doc """
  Returns the list of accounts by user.

  ## Examples

      iex> list(user)
      [%Account{}, ...]

  """
  def list(user) do
    from(a in Account, where: a.user_id == ^user.id, preload: [:user])
    |> Repo.all()
    |> Repo.preload(:user)
  end

  @doc """
  Gets a single account.

  Raises `Ecto.NoResultsError` if the Account does not exist.

  ## Examples

      iex> get!(123)
      %Account{}

      iex> get!(456)
      ** (Ecto.NoResultsError)

  """
  def get!(id), do: Repo.get!(Account, id)

  @doc """
  Gets a single account by user and some field.

  Raises `Ecto.NoResultsError` if the Account does not exist.

  ## Examples

      iex> get_by!(user, :identity, "BGH66")
      %Account{}

      iex> get_by!(user, :identity, "XXGH5")
      ** (Ecto.NoResultsError)

  """
  def get_by!(%User{} = user, field, value) do
    query = from(a in Account, where: a.user_id == ^user.id and field(a, ^field) == ^value, preload: [:user])
    Repo.one!(query)
  end

  @doc """
  Gets a single account by user.

  Raises `Ecto.NoResultsError` if the Account does not exist.

  ## Examples

      iex> get_by!(user)
      %Account{}

      iex> get_by!(user)
      ** (Ecto.NoResultsError)

  """
  def get_by!(field, value) do
    query = from(a in Account, where: field(a, ^field) == ^value, preload: [:user])
    Repo.one!(query)
  end

  @doc """
  Creates a account.

  ## Examples

      iex> create(%{identity: "FGB54", balance: "1234.78"})
      {:ok, %Account{}}

      iex> create(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create(User.t(), map()) :: {:ok, Account.t()} | {:error, %Ecto.Changeset{}}
  def create(user, attrs) do
    try do
      attrs = Map.merge(attrs, %{balance: Decimal.new(attrs.balance)})

      Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: attrs='#{inspect(attrs)}")

      %Account{}
      |> Account.changeset(user, attrs)
      |> Repo.insert()
    rescue
      error ->
       {:error, error}
    end
  end

  @doc """
  Generate a random identity.

  ## Examples

      iex> gen_identity()
      "GBH33"

  """
  def gen_identity() do
    min = String.to_integer("100000", 36)
    max = String.to_integer("ZZZZZZ", 36)

    max
    |> Kernel.-(min)
    |> :rand.uniform()
    |> Kernel.+(min)
    |> Integer.to_string(36)
  end

  @doc """
  Updates a account.

  ## Examples

      iex> update(account, %{field: new_value})
      {:ok, %Account{}}

      iex> update(account, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update(%Account{} = account, attrs) do
    try do
      attrs = Map.merge(attrs, %{balance: Decimal.new(attrs.balance)})

      Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: attrs='#{inspect(attrs)}")
      account
      |> Ecto.Changeset.change(attrs)
      |> Repo.update()
    rescue
      error ->
       {:error, error}
    end
  end

  @doc """
  Deletes a account.

  ## Examples

      iex> delete(account)
      {:ok, %Account{}}

      iex> delete(account)
      {:error, %Ecto.Changeset{}}

  """
  def delete(%Account{} = account) do
    Repo.delete(account)
  end
end
