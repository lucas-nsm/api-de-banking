defmodule Bank.Transactions do
  @moduledoc """
  The Transactions context.
  """

  import Ecto.Query, warn: false
  alias Bank.Repo
  alias Bank.Accounts.Transaction
  alias Bank.Accounts.Account
  alias Bank.Accounts

  require Logger

  @doc """
  Returns the list of transactions.

  ## Examples

      iex> list()
      [%Transaction{}, ...]

  """
  def list do
    Repo.all(Transaction)
  end

  @doc """
  Gets a single transaction.

  Raises `Ecto.NoResultsError` if the Transaction does not exist.

  ## Examples

      iex> get!(123)
      %Transaction{}

      iex> get!(456)
      ** (Ecto.NoResultsError)

  """
  def get!(id), do: Repo.get!(Transaction, id)

  @doc """
  Returns the sum of transactions by date.

  ## Examples

      iex> get_total(account, "2020-8")
      {:ok, "124.50"}

      iex> get_total(account, "20208")
      {:error, :invalid_format}

  """
  def get_total(%Account{} = account, date) do
    Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: identity='#{account.identity}'" <>
                " date='#{date}'")
    filter =
      case String.split(date, "-") do
        [year, month, day] when byte_size(year) == 4 and byte_size(month) == 2 and byte_size(day) == 2 ->
          build_filter_day(account.id, date)

        [year, month] when byte_size(year) == 4 and byte_size(month) == 2 -> 
          build_filter_month(account.id, year, month)

        [year] when byte_size(year) == 4 -> 
          build_filter_year(account.id, year)

        [year] when byte_size(year) == 0 ->
          build_filter_total(account.id)

        _ ->
          {:error, :invalid_format}
      end

    {:ok, Repo.one(from t in Bank.Accounts.Transaction, 
                   where: ^filter,
                   select: sum(t.amount))
    }
  end

  defp build_filter_day(account_id, date) do
    dynamic([t], t.account_id == ^account_id and
                 fragment("?::date", t.inserted_at) == ^Date.from_iso8601!(date))

  end

  defp build_filter_month(account_id, year, month) do
    [year, month] = [String.to_integer(year), String.to_integer(month)]
    {:ok, start_date} = Date.new(year, month, 1)
    {:ok, temp_date} = Date.new(year, month + 1, 1)
    end_date = Date.add(temp_date, ((temp_date.day) * -1))

    dynamic([t], t.account_id == ^account_id and
                 fragment("?::date", t.inserted_at) >= ^start_date and 
                 fragment("?::date", t.inserted_at) <= ^end_date)
  end

  defp build_filter_year(account_id, year) do
    [year] = [String.to_integer(year)]
    {:ok, start_date} = Date.new(year, 1, 1)
    {:ok, end_date} = Date.new(year, 12, 31)

    dynamic([t], t.account_id == ^account_id and
                 fragment("?::date", t.inserted_at) >= ^start_date and 
                 fragment("?::date", t.inserted_at) <= ^end_date)
  end

  defp build_filter_total(account_id) do
    dynamic([t], t.account_id == ^account_id)
  end

  @doc """
  Creates a withdraw transaction.

  ## Examples

      iex> withdraw(account, %{"amount" : "123.40"})
      {:ok, %Transaction{}}

      iex> withdraw(account, %{"amount" : "1230.40"})
      {:error, :insufficient_funds}

  """
  def withdraw(%Account{} = account, %{amount: amount} = attrs) do
    Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: identity='#{account.identity}' (" 
                <> inspect(account.balance) <> " - " <> inspect(amount) <> " = " 
                <> inspect(Decimal.sub(account.balance, amount)) <> ")")

    with {_, ""} <- Float.parse(amount),
         true <- (Decimal.new(amount) |> Decimal.positive?()) do

      case Decimal.sub(account.balance, amount)
           |> Decimal.compare("0") do
        balance_result when balance_result != :lt ->

          Accounts.update(account, %{balance: Decimal.sub(account.balance, amount)})
          attrs = Map.merge(attrs, %{amount: Decimal.mult(amount, -1)})
          create(account, attrs)

        _ ->
          Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: insufficient funds")
          {:error, :insufficient_funds}
      end
    else 
      _ ->
        {:error, :invalid_format}
    end
  end

  @doc """
  Creates a deposit transaction.

  ## Examples

      iex> deposit(account, %{"amount" : "123.40"})
      {:ok, %Transaction{}}

      iex> deposit(account, %{"amount" : "123.40"})
      {:error, %Ecto.Changeset{}}

  """
  def deposit(%Account{} = account, %{amount: amount} = attrs) do
    Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: identity='#{account.identity}' ("
                <> inspect(account.balance) <> " + " <> inspect(amount) <> " = " 
                <> inspect(Decimal.add(account.balance, amount)) <> ")")


    with {_, ""} <- Float.parse(amount),
         true <- (Decimal.new(amount) |> Decimal.positive?()) do

      Accounts.update(account, %{balance: Decimal.add(account.balance, amount)})
      create(account, attrs)
    else 
      _ ->
        {:error, :invalid_format}
    end
  end

  @doc """
  Creates a transfer transaction.

  ## Examples

      iex> transfer(account, %{"amount" : "123.40", "dst_account" : "BJK43"})
      {:ok, %Transaction{}}

      iex> transfer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec transfer(Account.t(), map()) :: {:ok, Account.t()} | {:error, String.t()}
  def transfer(%Account{} = src_account, attrs) do
    try do
      with dst_account <- Accounts.get_by!(:identity, attrs.dst_account),
           {:ok, %Transaction{} = transaction} <- withdraw(src_account, attrs),
           {:ok, _} <- deposit(dst_account, attrs) do
        {:ok, transaction}
      else
        {:error, message} ->
          {:error, message}
      end
    rescue 
      _error ->
        {:error, :not_found}
    catch kind, error ->
      Logger.error("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: #{kind}, #{error}")
      {:error, :invalid_format}
    end
  end

  @doc """
  Creates a transaction.

  ## Examples

      iex> create(%{field: value})
      {:ok, %Transaction{}}

      iex> create(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create(%Account{} = account, attrs \\ %{}) do
    %Transaction{}
    |> Transaction.changeset(account, attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a transaction.

  ## Examples

      iex> delete(transaction)
      {:ok, %Transaction{}}

      iex> delete(transaction)
      {:error, %Ecto.Changeset{}}

  """
  def delete(%Transaction{} = transaction) do
    Repo.delete(transaction)
  end
end
