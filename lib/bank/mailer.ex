defmodule Bank.Mailer do
  @moduledoc """
  The Mailer context.
  """
  require Logger

  @doc """
  Send email.

  ## Examples

      iex> send_email(
        %{
          to: auser@email.com, 
          from: "api_banking@email.com", 
          subject: "Saque realizado com sucesso", 
          text: "Saque de '10.29' realizado na conta '3U4B6C' em '2020-10-07 17:42:35'"
        }
      )
  """
  def send_email(attrs) do
    try do
      #Some framework with async task 
      Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: sending email, #{inspect(attrs)}")
      Logger.info("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: email sent!")

    rescue
      error ->
        Logger.error("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: #{error}")

    catch kind, error ->
      Logger.error("#{__MODULE__}:#{Atom.to_string(__ENV__.function |> elem(0))} :: #{kind}, #{error}")
    end

  end
end


