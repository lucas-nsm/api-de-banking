defmodule Bank.AccountsHelper do
  @moduledoc false

  alias Bank.{Accounts, Accounts.Account}

  @account_attrs %{
      balance: "100.23",
      identity: "HNF56"
  }

  def create(user, attrs \\ @account_attrs) do
    attrs = Map.merge(@account_attrs, attrs)
    {:ok, %Account{} = account} = Accounts.create(user, attrs)
    account
  end
end
