defmodule Bank.UsersHelper do
  @moduledoc false

  alias Bank.{Users, Accounts.User}

  @user_attrs %{
      email: "auser@email.com",
      password: "pass_a"
  }

  def create(attrs \\ @user_attrs) do
    attrs = Map.merge(@user_attrs, attrs)
    {:ok, %User{} = user} = Users.create(attrs)
    user
  end
end
