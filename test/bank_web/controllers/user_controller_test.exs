defmodule BankWeb.UserControllerTest do
  use BankWeb.ConnCase

  alias Bank.UsersHelper

  @create_attrs %{
    email: "auser@email.com",
    password: "pass_a"
  }
  @invalid_attrs %{
    email: "email.com",
    password: ""
  }

  @invalid_password %{
    email: "auser@email.com",
    password: "pass_b"
  }

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "POST /api/users/signup" do
    test "renders user when data is valid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :create), @create_attrs)
      assert %{"email" => email, "token" => token} = json_response(conn, 201)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :create), @invalid_attrs)
      assert %{"email" => ["has invalid format"], "password" => ["can't be blank"]} = json_response(conn, 422)["errors"]
    end
  end

  describe "POST /api/users/signin" do
    setup [:create_user]

    test "renders user when password is valid", %{conn: conn, user: _user} do
      conn = post(conn, Routes.user_path(conn, :signin), @create_attrs)
      assert %{"email" => email, "token" => token} = json_response(conn, 201)
    end

    test "renders errors when password is invalid", %{conn: conn, user: _user} do
      conn = post(conn, Routes.user_path(conn, :signin), @invalid_password)
      assert %{"detail" => "Unauthorized"} == json_response(conn, 401)["errors"]
    end
  end

  defp create_user(_) do
    user = UsersHelper.create(@create_attrs)
    %{user: user}
  end
end
