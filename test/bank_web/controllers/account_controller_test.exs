defmodule BankWeb.AccountControllerTest do
  use BankWeb.ConnCase

  alias Bank.{AccountsHelper, UsersHelper}

  @create_attrs %{
    balance: "100.23",
    identity: "HNF56"
  }
  @update_attrs %{
    balance: "134.34",
    identity: "GGJ11"
  }

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "POST /api/accounts/create" do
    setup do
      user = UsersHelper.create()
      {:ok, user: user, conn: create_authentication(user)}
    end

    test "create account", %{conn: conn} do
      conn = post(conn, Routes.account_path(conn, :create))
      assert %{"balance" => _, "identity" => _} = json_response(conn, 201)["data"]
    end
  end

  describe "GET /api/accounts" do
    setup [:create_user_auth_accounts]

    test "lists all accounts", %{conn: conn} do
      conn = get(conn, Routes.account_path(conn, :index))
      assert length(json_response(conn, 200)["data"]) == 2
    end
  end

  describe "GET /api/accounts/:identity" do
    setup [:create_user_auth_accounts]

    test "lists one valid account", %{conn: conn} do
      conn = get(conn, Routes.account_path(conn, :show, "HNF56"))
      assert json_response(conn, 200)["data"] == %{"balance" => "100.23", "identity" => "HNF56"}
    end

    test "lists one invalid account", %{conn: conn} do
      conn = get(conn, Routes.account_path(conn, :show, "GGG99"))
      assert json_response(conn, 404)["errors"] == %{"detail" => "Not Found"}
    end
  end

  defp create_user_auth_accounts(_) do
    user = UsersHelper.create()
    AccountsHelper.create(user, @create_attrs)
    AccountsHelper.create(user, @update_attrs)
    {:ok, user: user, conn: create_authentication(user)}
  end

end
