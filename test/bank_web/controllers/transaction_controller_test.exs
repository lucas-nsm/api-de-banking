defmodule BankWeb.TransactionControllerTest do
  use BankWeb.ConnCase

  alias Bank.Transactions
  alias Bank.{AccountsHelper, UsersHelper}

  @create_attrs %{
    amount: "42"
  }

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "GET /api/transactions/:identity/:period" do
    setup [:create_user_auth_accounts]

    test "lists all transactions", %{conn: conn, account_a: account_a} do

      Transactions.withdraw(account_a, %{amount: "100.00"})

      conn = get(conn, Routes.transaction_path(conn, :show, account_a.identity))
      assert json_response(conn, 200) == %{"identity" => "ABCD12", "period" => "total", "total" => "-100.00"}
    end

    test "lists day transactions", %{conn: conn, account_a: account_a} do

      Transactions.withdraw(account_a, %{amount: "100.00"})

      date = current_date()
      conn = get(conn, Routes.transaction_path(conn, :show, account_a.identity), %{date: date})
      assert json_response(conn, 200) == %{"identity" => "ABCD12", "period" => date, "total" => "-100.00"}
    end

    test "lists invalid period", %{conn: conn, account_a: account_a} do

      Transactions.withdraw(account_a, %{amount: "100.00"})

      conn = get(conn, Routes.transaction_path(conn, :show, account_a.identity), %{date: "234"})
      assert json_response(conn, 400)["errors"] == %{"detail" => "Invalid format"}
    end
  end

  describe "POST /api/transactions/withdraw/:identity" do
    setup [:create_user_auth_accounts]

    test "renders transaction withdraw when data is valid", %{conn: conn, account_a: account_a} do
      conn = post(conn, Routes.transaction_path(conn, :withdraw, account_a.identity), @create_attrs)
      assert %{"amount" => "-42.00"} = json_response(conn, 201)["data"]
    end

    test "renders transaction withdraw with insufficient funds account", %{conn: conn, account_a: account_a} do
      conn = post(conn, Routes.transaction_path(conn, :withdraw, account_a.identity), %{amount: "1500"})
      assert json_response(conn, 400)["errors"] == %{"detail" => "Insufficient funds"}
    end

    test "renders transaction withdraw with negative amount", %{conn: conn, account_a: account_a} do
      conn = post(conn, Routes.transaction_path(conn, :withdraw, account_a.identity), %{amount: "-100"})
      assert json_response(conn, 400)["errors"] == %{"detail" => "Invalid format"}
    end
  end

  describe "POST /api/transactions/deposit/:identity" do
    setup [:create_user_auth_accounts]

    test "renders transaction deposit when data is valid", %{conn: conn, account_a: account_a} do
      conn = post(conn, Routes.transaction_path(conn, :deposit, account_a.identity), @create_attrs)
      assert %{"amount" => "42.00"} = json_response(conn, 201)["data"]
    end

    test "renders transaction deposit with invalid account", %{conn: conn} do
      conn = post(conn, Routes.transaction_path(conn, :deposit, "FJND56"), @create_attrs)
      assert json_response(conn, 404)["errors"] == %{"detail" => "Not Found"}
    end

    test "renders transaction deposit with negative amount", %{conn: conn, account_a: account_a} do
      conn = post(conn, Routes.transaction_path(conn, :withdraw, account_a.identity), %{amount: "-100"})
      assert json_response(conn, 400)["errors"] == %{"detail" => "Invalid format"}
    end
  end

  describe "POST /api/transactions/transfer/:identity" do
    setup [:create_user_auth_accounts]

    test "renders transaction transfer when data is valid", %{conn: conn, account_a: account_a, account_b: account_b} do
      conn = post(
        conn, 
        Routes.transaction_path(
          conn, :transfer, 
          account_a.identity), 
          %{amount: "123", dst_account: account_b.identity}
      )
      assert %{"amount" => "-123.00"} = json_response(conn, 201)["data"]
    end

    test "renders transaction transfer with negative amount", %{conn: conn, account_a: account_a, account_b: account_b} do
      conn = post(
        conn,
        Routes.transaction_path(
          conn, :transfer,
          account_a.identity),
          %{amount: "-123", dst_account: account_b.identity}
      )
      assert json_response(conn, 400)["errors"] == %{"detail" => "Invalid format"}
    end

    test "renders transaction transfer with invalid amount", %{conn: conn, account_a: account_a, account_b: account_b} do
      conn = post(
        conn,
        Routes.transaction_path(
          conn, :transfer,
          account_a.identity),
          %{amount: "Inf", dst_account: account_b.identity}
      )
      assert json_response(conn, 400)["errors"] == %{"detail" => "Invalid format"}
    end
  end

  defp current_date() do
    date = NaiveDateTime.to_date(DateTime.utc_now())
    with {:ok, d} <- Date.new(date.year, date.month, date.day) do
      [d.year, d.month, d.day]
      |> Enum.map(&to_string/1)
      |> Enum.map(&String.pad_leading(&1, 2, "0"))
      |> Enum.join("-")
    end
  end

  defp create_user_auth_accounts(_) do
    user_a = UsersHelper.create(%{email: "auser@email.com"})
    account_a = AccountsHelper.create(user_a, %{identity: "ABCD12", balance: "1000"})

    user_b = UsersHelper.create(%{email: "buser@email.com"})
    account_b = AccountsHelper.create(user_b, %{identity: "DCBA99", balance: "1000"})

    {
      :ok, 
      user: user_a, 
      account_a: account_a, 
      account_b: account_b, 
      conn: create_authentication(user_a)
    }
  end

end
