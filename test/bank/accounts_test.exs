defmodule Bank.AccountsTest do
  use Bank.DataCase

  alias Bank.Accounts
  alias Bank.{AccountsHelper, UsersHelper}

  describe "accounts" do
    alias Bank.Accounts.Account

    @valid_attrs %{
      balance: "100.23", 
      identity: "HNF56"
    }

    @update_attrs %{
      balance: "134.34", 
      identity: "GGJ11"
    }

    @invalid_attrs %{
      balance: "FDF.43", 
      identity: "$%^&*"
    }

    test "list/0 returns all accounts" do
      UsersHelper.create()
      |> AccountsHelper.create(@valid_attrs)

      [account] = Accounts.list()
      assert account.balance == Decimal.new("100.23")
      assert account.identity == "HNF56"
    end

    test "get!/1 returns the account with given id" do
      %{id: id} = 
        UsersHelper.create()
        |> AccountsHelper.create(@valid_attrs)

      account = Accounts.get!(id)
      assert account.identity == "HNF56"
    end

    test "get_by!/2 returns the account with given id" do
      UsersHelper.create()
      |> AccountsHelper.create(@valid_attrs)

      account = Accounts.get_by!(:identity, "HNF56")
      assert account.identity == "HNF56"
    end

    test "get_by!/3 returns the account with given id" do
      user = UsersHelper.create()
      AccountsHelper.create(user, @valid_attrs)

      account = Accounts.get_by!(user, :identity, "HNF56")
      assert account.identity == "HNF56"
    end

    test "create_account/2 with valid data creates a account" do
      user_a = UsersHelper.create(%{email: "auser@email.com"})
      assert {:ok, %Account{} = _account} = Accounts.create(user_a, @valid_attrs)

      user_b = UsersHelper.create(%{email: "buser@email.com"})
      assert {:ok, %Account{} = _account} = Accounts.create(user_b, @update_attrs)

      account_a = Accounts.get_by!(:identity, "HNF56")
      account_b = Accounts.get_by!(:identity, "GGJ11")

      assert account_a.balance == Decimal.new("100.23")
      assert account_a.user.id == user_a.id

      assert account_b.balance == Decimal.new("134.34")
      assert account_b.user.id == user_b.id
    end

    test "update_account/2 with valid data updates the account" do
      account = 
        UsersHelper.create()
        |> AccountsHelper.create(@valid_attrs)

      assert {:ok, %Account{} = account} = Accounts.update(account, @update_attrs)
      assert account.balance == Decimal.new("134.34")
      assert account.identity == "GGJ11"
    end

    test "update_account/2 with invalid data returns error changeset" do
      account =
        UsersHelper.create()
        |> AccountsHelper.create(@valid_attrs)

      assert {:error, %Decimal.Error{}} = Accounts.update(account, @invalid_attrs)
    end

    test "delete_account/1 deletes the account" do
      assert {:ok, %Account{} = account} =
        UsersHelper.create()
        |> AccountsHelper.create(@valid_attrs)
        |> Accounts.delete()

      assert_raise Ecto.NoResultsError, fn -> Accounts.get!(account.id) end
    end
  end
end
