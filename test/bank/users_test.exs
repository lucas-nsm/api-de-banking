defmodule Bank.UsersTest do
  use Bank.DataCase

  alias Bank.Users

  describe "users" do
    alias Bank.Accounts.User

    @valid_attrs %{
      email: "auser@email.com", 
      password: "pass_a"
    }
    
    @update_attrs %{
      email: "buser@email.com", 
      password: "pass_b"
    }

    @invalid_attrs %{
      email: nil, 
      password: nil
    }

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Users.create()

      user
    end

    test "list/0 returns all users" do
      user =
        user_fixture()
        |>  struct(%{password: nil})

      assert Users.list() == [user]
    end

    test "get!/1 returns the user with given id" do
      user = user_fixture()
      assert Users.get!(user.id).email == user.email
    end

    test "create/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Users.create(@valid_attrs)
      assert user.email == "auser@email.com"
      assert is_binary(user.encrypted_password)
    end

    test "create/1 with invalid number of params" do
      assert {:ok, %User{} = user} = Users.create(@valid_attrs)
      assert user.email == "auser@email.com"
      assert is_binary(user.encrypted_password)
    end

    test "create/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.create(%{password: "pass_a"})
    end

    test "update/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Users.update(user, @update_attrs)
      assert user.email == "buser@email.com"
    end

    test "update/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Users.update(user, @invalid_attrs)
    end

    test "delete/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Users.delete(user)
      assert_raise Ecto.NoResultsError, fn -> Users.get!(user.id) end
    end
  end
end
