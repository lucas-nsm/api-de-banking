defmodule Bank.TransactionsTest do
  use Bank.DataCase

  alias Bank.Transactions
  alias Bank.Accounts
  alias Bank.{AccountsHelper, UsersHelper}

  describe "transactions" do
    alias Bank.Accounts.Transaction

    @valid_attrs %{amount: "1345.32"}
    @invalid_attrs %{amount: "345,56"}

    test "deposit/2 with valid data creates a transaction" do
      assert {:ok, %Transaction{} = transaction} =
        UsersHelper.create()
        |> AccountsHelper.create(%{balance: "0"})
        |> Transactions.deposit(@valid_attrs)

      transaction = Transactions.get!(transaction.id)
      assert transaction.amount == Decimal.new(@valid_attrs.amount)
      assert Accounts.get!(transaction.account_id).balance == Decimal.new(@valid_attrs.amount)
    end

    test "deposit/2 with invalid data error" do
      assert {:error, :invalid_format} =
        UsersHelper.create()
        |> AccountsHelper.create()
        |> Transactions.deposit(@invalid_attrs) 
    end

    test "withdraw/2 with invalid data error" do
      assert {:error, :invalid_format} =
        UsersHelper.create()
        |> AccountsHelper.create()
        |> Transactions.withdraw(@invalid_attrs) 
    end

    test "withdraw/2 with insufficient funds error" do
      assert {:error, :insufficient_funds} =
        UsersHelper.create()
        |> AccountsHelper.create(%{balance: "0"})
        |> Transactions.withdraw(@valid_attrs) 
    end

    test "transfer/2 with valid data creates a transaction" do
      account_a =
        UsersHelper.create(%{email: "auser@email.com"})
        |> AccountsHelper.create(%{balance: "1000", identity: Accounts.gen_identity()})

      account_b =
        UsersHelper.create(%{email: "buser@email.com"})
        |> AccountsHelper.create(%{balance: "1000", identity: Accounts.gen_identity()})

      assert {:ok, %Transaction{} = _transaction} = 
        Transactions.transfer(Accounts.get!(account_a.id), %{amount: "300", dst_account: Accounts.get!(account_b.id).identity})

      assert Accounts.get!(account_a.id).balance == Decimal.new("700")
      assert Accounts.get!(account_b.id).balance == Decimal.new("1300")
    end

    test "transfer/2 with invalid destination account error" do
      account_a =
        UsersHelper.create(%{email: "auser@email.com"})
        |> AccountsHelper.create(%{balance: "1000", identity: Accounts.gen_identity()})

      assert {:error, :not_found} = 
        Transactions.transfer(Accounts.get!(account_a.id), %{amount: "300", dst_account: "FSD45"})

      assert Accounts.get!(account_a.id).balance == Decimal.new("1000")
    end

    test "transfer/2 with insufficient funds error" do
      account_a =
        UsersHelper.create(%{email: "auser@email.com"})
        |> AccountsHelper.create(%{balance: "1000", identity: Accounts.gen_identity()})

      account_b =
        UsersHelper.create(%{email: "buser@email.com"})
        |> AccountsHelper.create(%{balance: "1000", identity: Accounts.gen_identity()})

      assert {:error, :insufficient_funds} =
        Transactions.transfer(Accounts.get!(account_a.id), %{amount: "1500", dst_account: Accounts.get!(account_b.id).identity})

      assert Accounts.get!(account_a.id).balance == Decimal.new("1000")
      assert Accounts.get!(account_b.id).balance == Decimal.new("1000")
    end

    test "get_total/2 with valid data show total transactions" do
      account_a =
        UsersHelper.create(%{email: "auser@email.com"})
        |> AccountsHelper.create(%{balance: "1000", identity: Accounts.gen_identity()})

      account_b =
        UsersHelper.create(%{email: "buser@email.com"})
        |> AccountsHelper.create(%{balance: "1000", identity: Accounts.gen_identity()})

      assert {:ok, %Transaction{} = _transaction} = 
        Transactions.transfer(Accounts.get!(account_a.id), %{amount: "300", dst_account: Accounts.get!(account_b.id).identity})

      assert Accounts.get!(account_a.id).balance == Decimal.new("700")
      assert Accounts.get!(account_b.id).balance == Decimal.new("1300")

      assert {:ok, %Transaction{} = _transaction} = 
        Transactions.transfer(Accounts.get!(account_b.id), %{amount: "50", dst_account: Accounts.get!(account_a.id).identity})

      assert Accounts.get!(account_a.id).balance == Decimal.new("750")
      assert Accounts.get!(account_b.id).balance == Decimal.new("1250")

      assert {:ok, value_a} = Transactions.get_total(account_a, "")
      assert {:ok, value_b} = Transactions.get_total(account_b, "")

      assert value_a == Decimal.new("-250")
      assert value_b == Decimal.new("250")
    end

    test "deposit/2 with floating point math" do
      account = 
        UsersHelper.create()
        |> AccountsHelper.create(%{balance: "0"})

      Transactions.deposit(account, %{amount: "0.1"})

      account = Accounts.get!(account.id)

      Transactions.deposit(account, %{amount: "0.2"})

      account = Accounts.get!(account.id)

      assert account.balance  == Decimal.new("0.3")
      {:ok, value} = Transactions.get_total(account, "")
      assert value == Decimal.new("0.3")
    end
  end
end
